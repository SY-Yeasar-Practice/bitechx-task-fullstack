import {
  Entity,
  BaseEntity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
} from "typeorm";
import {
  IsAlpha,
  IsBoolean,
  IsDefined,
  IsEmpty,
  Matches,
} from "class-validator";

@Entity({ name: "users" })
export class Users extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: "integer",
  })
  public userId!: number; //col - 1

  @Column({
    type: "varchar",
    length: 25,
    name: "userName",
  })
  @IsDefined()
  @IsEmpty({
    always: true,
    message: "User Name required!!!",
  })
  @IsAlpha()
  public userName!: string; //col - 2

  @Column({
    type: "varchar",
    length: 200,
    name: "password",
  })
  @IsEmpty({
    always: true,
    message: "Password required!!!",
  })
  @IsDefined()
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/, {
    message: "Password pattern not match",
  })
  public password!: string; //col - 3

  @Column({
    type: "boolean",
    default: false,
  })
  @IsBoolean({ message: "Is Delete will be Boolean" })
  public isDelete!: string; //col - 4

  @Column({
    type: "boolean",
    default: true,
  })
  @IsBoolean({ message: "Is Active will be Boolean" })
  public isActive!: string; //col - 5

  @CreateDateColumn({
    name: "createAt",
  })
  public createAt!: string; //col - 6

  @UpdateDateColumn({
    name: "updateAt",
  })
  public updateAt!: string; //col - 7
}
