import { Router } from "express";
import {
  registerNewUserHandler,
  updateUserByIdHandler,
  deleteUserByIdHandler,
  getAllUserHandler,
} from "../controller/user";

const route = Router();

route.post("/registration", registerNewUserHandler);
route.post("/update/:userId", updateUserByIdHandler);

route.put("/delete/:userId", deleteUserByIdHandler);

route.get("/get/all", getAllUserHandler);

export default route;
