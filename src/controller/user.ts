import { NextFunction, Request, Response } from "express";
import bcrypt from "bcrypt";
import { validate } from "class-validator";

//utils modules will import here

//dto input validation  module
import { DtoUserRegistration } from "../dto/users/registration.dto";
import { Users } from "../entites/User";
import { DtoUserUpdate } from "../dto/users/updateUser.dto";

//local type
type Body = (req: Request, res: Response, next: NextFunction) => Promise<void>; //body type

//register a new user
const registerNewUserHandler: Body = async (req, res) => {
  try {
    const registrationData: DtoUserRegistration = req.body; //take the data from body

    //validation part start from here
    const checkRegistrationData: DtoUserRegistration | any =
      new DtoUserRegistration();
    for (const property in req.body) {
      checkRegistrationData[property] = req.body[property];
    } //store all body data dynamically into the validation instance
    const isValidationError = await validate(checkRegistrationData); //validate the input data here
    if (isValidationError.length) {
      //if validation failed
      res.json({
        message: isValidationError,
        status: 402,
      });
    } else {
      //password and confirm password validation
      registrationData.password != registrationData.confirmPassword &&
        res.json({
          message: "Confirm Password does not match with password",
          status: 406,
        });
      //validation part end here

      const encryptedPassword: string = await bcrypt.hash(
        registrationData.password,
        10
      );
      const newUsers: Users | any = await new Users(); //create a instance of users
      for (const property in registrationData) {
        //store the users data dynamically
        property != "confirmPassword" &&
          (newUsers[property] = req.body[property]);
      } //insert data into user
      newUsers.password = encryptedPassword; //insert the encrypted password
      const saveNewUsers: Users = await newUsers.save(); //save the user here
      if (Object.keys(saveNewUsers).length) {
        //if user successfully save
        res.json({
          message: "User successfully saved",
          status: 201,
        });
      } else {
        res.json({
          message: "User failed to save",
          status: 400,
        });
      }
    }
  } catch (err) {
    console.log(err);
    res.json({
      message: "Internal Error!!",
      status: 406,
    });
  }
};

//update user by user id
const updateUserByIdHandler: Body = async (req, res) => {
  try {
    const updateDtoData: DtoUserUpdate = req.body; //take the data from body

    //validation part start from here
    const checkUpdateData: DtoUserUpdate | any = new DtoUserUpdate();
    for (const property in req.body) {
      checkUpdateData[property] = req.body[property];
    } //store all body data dynamically into the validation instance
    const isValidationError = await validate(checkUpdateData); //validate the input data here
    if (isValidationError.length) {
      //if validation failed
      res.json({
        message: isValidationError,
        status: 402,
      });
    } else {
      const updateUser = await Users.createQueryBuilder()
        .update(Users)
        .set({ userName: updateDtoData.userName })
        .where("userId = :userId", { userId: req.params.userId })
        .execute();

      if (updateUser.affected) {
        res.json({
          message: "User update successfully ",
          status: 202,
        });
      } else {
        res.json({
          message: "User update failed",
          status: 400,
        });
      }
    }
  } catch (err) {
    console.log(err);
    res.json({
      message: "Internal Error!!",
      status: 406,
    });
  }
};

//delete user by userId
const deleteUserByIdHandler: Body = async (req, res) => {
  try {
    const deleteUser = await Users.createQueryBuilder()
      .delete()
      .from(Users)
      .where("userId = :userId", { userId: req.params.userId })
      .execute();

    if (deleteUser.affected == 0) {
      res.json({
        message: "User Not found ",
        status: 404,
      });
    } else {
      res.json({
        message: "User have successfully deleted",
        status: 200,
      });
    }
  } catch (err) {
    console.log(err);
    res.json({
      message: "Internal Error!!",
      status: 405,
    });
  }
};

//get all user from database
const getAllUserHandler: Body = async (req, res) => {
  try {
    const getUsers = await Users.createQueryBuilder("user")
      .select("user.userName")
      .addSelect("user.createAt")
      .getMany();
    if (getUsers.length) {
      res.json({
        message: `${getUsers.length} user found`,
        users: getUsers,
        status: 200,
      });
    } else {
      res.json({
        users: null,
        message: "User Not found ",
        status: 404,
      });
    }
  } catch (err) {
    console.log(err);
    res.json({
      message: "Internal Error!!",
      users: null,
      status: 405,
    });
  }
};

export {
  registerNewUserHandler,
  updateUserByIdHandler,
  deleteUserByIdHandler,
  getAllUserHandler,
};
