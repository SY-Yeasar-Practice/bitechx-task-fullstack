import { IsDefined, IsString, Matches, Validate } from "class-validator";
import {
  PasswordValidation,
  PasswordValidationRequirement,
} from "class-validator-password-check";

const passwordRequirement: PasswordValidationRequirement = {
  mustContainLowerLetter: true,
  mustContainNumber: true,
  mustContainSpecialCharacter: true,
  mustContainUpperLetter: true,
};

export class DtoUserRegistration {
  @IsString({
    message: "userName must be string",
  })
  @IsDefined({
    message: "userName required!!!",
  })
  public userName!: string; // one

  @IsString({
    message: "Password must be string",
  })
  @IsDefined({
    message: "Password required!!!",
  })
  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/, {
    message: "Password pattern not match",
  })
  @Validate(PasswordValidation, [passwordRequirement])
  public password!: string; //Two  //password contains at least 8 char and at least one upper case and one lowercase

  @IsString({
    message: "Confirm Password must be string",
  })
  @IsDefined({
    message: "Confirm Password required!!!",
  })
  public confirmPassword!: string; //Three
}
