import { IsDefined, IsString } from "class-validator";

export class DtoUserUpdate {
  @IsString({
    message: "userName must be string",
  })
  @IsDefined({
    message: "userName required!!!",
  })
  public userName!: string; // one
}
