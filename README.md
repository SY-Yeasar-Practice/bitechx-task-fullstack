# BiTechX-Task

This is a simple CURD based app

## Key Technologies

**Server-Side:** Node JS, Express JS

**Database:** MySQL (with ORM TypeORM)

**Language:** TypeScript (server)

## Key Roles

- User

## Key Features

- Create a new user by give userName and password
- Delete existing user by using userId
- Update existing user by using userId
- Get all existing users

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/SY-Yeasar-Practice/bitechx-task-fullstack.git
```

Go to the project directory

```bash
  cd BiTechX-Task-FullStack
```

Install dependencies

```bash
  npm install || npm i
```

Start the server

```bash
  npm run dev
```

## Installation

Install my-project with npm

```bash
  npm install || npm i
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

**`PORT`** //this will be the server port

**`TYPEORM_CONNECTION`** //this will be data base type example: mysql

**`TYPEORM_HOST`** //this will be the hosting server Example: localhost

**`TYPEORM_PORT`** //this will be the database server port Example: 3306 (default for xampp)

**`TYPEORM_USERNAME`** //this will be the databse user name Example: `root` for xampp sever default

**`TYPEORM_DATABASE`** //this will be the databse name. At first user need to create a database and then insert the name here Example:airQuality (a random databse name)

**`TYPEORM_SYNCHRONIZE`** //this will sync the databse table and build the table. It has two value `true or false` default value is `false`

**`TYPEORM_LOGGING`**//this will print the sql raw query in the CLI. It has two value `true or false` default value is `false`

**`TYPEORM_ENTITIES`** //this will denote the directory of all entities Example: `./src/entites/**/*.ts`

**`TYPEORM_SUBSCRIBERS`** //this will denote the directory of all subscriber Example: `./src/subscriber/**/*.ts`

**`TYPEORM_MIGRATIONS`** //this will denote the directory of all migration Example: `./src/migration/**/*.ts`

**`TYPEORM_ENTITIES_DIR`** // dentoe the entities directory example; ` src/model`

**`TYPEORM_MIGRATIONS_DIR`** // dentoe the entities directory example; ` src/migration`

**`TYPEORM_SUBSCRIBERS_DIR`** // dentoe the entities directory example; ` src/subscriber`

**`SERVER_BASE_URL`** //this will be the server's base url. Example: `"http://localhost:3030"`

**`CORS_ORIGIN`** //this will be the cors origin basicially the client site base url. Exapmle : `"http://localhost:3000"` Deafult base url of next js

## Documentation

[POST_MAN](https://www.postman.com/red-trinity-151066/workspace/bitechx-task-fullstack)

## Support

For support, sadmanishopnil@gmail.com
